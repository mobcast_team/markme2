'use strict';
const HttpErrors = require('http-errors');

module.exports = function(Organization) {
  Organization.afterRemote('*.__create__admin', function(ctx, inst, next) {
    let Role = Organization.app.models.Role;
    let RoleMapping = Organization.app.models.RoleMapping;
    let LeaveType = Organization.app.models.LeaveType;

    // Assign orgManager role
    Role.findOne({
      where: {
        name: 'orgAdmin',
      },
    }, function(err, role) {
      if (err)
        throw err;
      RoleMapping.findOne({
        where: {
          principalId: inst.id,
        },
      }, function(err, principal) {
        if (principal) {
          principal.updateAttribute({roleId: inst.roleId});
        } else {
          RoleMapping.create({
            principalType: 'Employee',
            principalId: inst.id,
            roleId: role.id,
          }, function(err, principal) {
            if (err)
              throw err;
            console.log('Created Principal: ', principal);
          });
        }
      });
    });

    // Assign a generic leave type to keep the DB from being empty
    Organization.findOne({
      where: {
        id: ctx.instance.id,
      },
      include: 'leaveTypes',
    }, (err, org) => {
      if (err)
        throw err;
      org.leaveTypes.count((err, count) => {
        if (count == 0) {
          LeaveType.create({
            name: 'Sick Leave',
            allowedLeaves: 5,
            organizationId: ctx.instance.id,
          }, (err, leaveType) => {
            if (err)
              throw err;
            console.log('Created leave type: ', leaveType);
          });
        }
      });
    });
    next();
  });

  Organization.getStatistics = function(id, cb) {
    let Employee = Organization.app.models.Employee;
    let Group = Organization.app.models.Group;

    Employee.count({
      where: {
        organizationId: id,
      },
    }, (err, empCount) => {
      if (err) cb(err);
      // We have the employee count here

      Group.find({
        where: {
          organizationId: id,
        },
        include: 'members',
      }, (err, groups) => {
        if (err) cb(err);


      });
    });
  };

  Organization.createGroupAdmin = function(orgId, groupId, employee, cb) {
    let Employee = Organization.app.models.Employee;
    let Group = Organization.app.models.Group;
    let Role = Organization.app.models.Role;
    let RoleMapping = Organization.app.models.RoleMapping;

    // Find the right organization
    Organization.findOne({
      where: {
        id: orgId,
      },
    }, function(err, org) {
      if (err) {
        return cb(err);
      }
      if (!org) {
        return cb(new HttpErrors.BadRequest('Org not found.'));
      }

      // Find the right group
      Group.findOne({
        where: {
          id: groupId,
        },
      }, (err, grp) => {
        if (err) {
          return cb(err);
        }
        if (!grp) {
          return cb(new HttpErrors.BadRequest('Group not found.'));
        }

        employee.groupId = grp.id;
        Employee.create(employee, (err, emp) => {
          if (err) {
            return cb(err);
          }
          console.log('Group admin created');
          // Assign Role (Group Admin)
          Role.findOne({
            where: {
              name: 'groupAdmin',
            },
          }, (err, role) => {
            if (err) {
              return cb(err);
            }
            RoleMapping.create({
              principalType: 'Employee',
              principalId: employee.id,
              roleId: role.id,
            }, function(err, principal) {
              if (err)
                throw err;
              console.log('Created Principal: ', principal);
              cb(null, employee);
            });
          });
        });
      });
    });
  };

  Organization.remoteMethod('createGroupAdmin', {
    accepts: [
      {
        arg: 'orgId',
        type: 'String',
        required: true,
      }, {
        arg: 'groupId',
        type: 'String',
        required: true,
      }, {
        arg: 'employee',
        type: 'Employee',
        required: true,
        http: {
          source: 'body',
        },
      },
    ],
    http: {
      path: '/:orgId/groups/:groupId/createGroupAdmin',
      verb: 'put',
    },
    returns: {
      arg: 'employee',
      type: 'Employee',
    },
  });
};
