'use strict';
const HttpErrors = require('http-errors');

module.exports = function(Leave) {
  Leave.observe('before save', function validateLeaveType(ctx, next) {
    let LeaveType = Leave.app.models.LeaveType;
    let type = ctx.instance.type;

    // Validate leave type
    if (type.id && type.organizationId && type.name) {
      LeaveType.findOne({
        where: {
          id: type.id,
          organizationId: type.organizationId,
        },
      }, (err, leaveType) => {
        if (err) {
          next(err);
        }
        if (!leaveType) {
          next(new HttpErrors.BadRequest('Invalid leave type'));
        } else {
          next();
        }
      });
    } else {
      next(new HttpErrors.BadRequest('Invalid leave type'));
    }
  });

  Leave.observe('before save', function validateLeave(ctx, next) {
    // Ensure isApproved is false
    ctx.instance.isApproved = false;

    // Check if leave has already been applied for today.
    let now = new Date();
    let today = new Date(
      Date.UTC(now.getUTCFullYear(), now.getMonth(), now.getUTCDate())
    );
    let tomorrow = new Date(
      Date.UTC(now.getUTCFullYear(), now.getMonth(), now.getUTCDate() + 1)
    );
    Leave.findOne({
      where: {
        date: {
          between: [today, tomorrow],
        },
      },
    }, (err, leave) => {
      if (err) {
        next(err);
      }
      if (leave) {
        console.log(leave);
        next(new HttpErrors.BadRequest(
          'You have already applied for a leave for this day'
        ));
      } else {
        next();
      }
    });
  });
};
