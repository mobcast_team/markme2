'use strict';
var path = require('path');

module.exports = function(Employee) {
  Employee.mobileLogin = function(credentials, callback) {
    if (!credentials.mobile || !credentials.password) {
      let error = new Error('Please enter the mobile no & password.');
      error.statusCode = 400;
      callback(error);
    }
    Employee.findOne({
      where: {
        mobile: credentials.mobile,
      },
      include: ['roles', 'organization'],
    }, function(finderr, userData) {
      if (finderr)
        callback(finderr);
      if (userData) {
        Employee.login({
          'username': userData.username,
          'password': credentials.password,
        }, (loginError, loginToken) => {
          if (loginError)
            callback(loginError);
          callback(null, {
            accessToken: loginToken,
            employee: userData,
          });
        });
      } else {
        let error = new Error('Login failed');
        error.statusCode = 401;
        callback(error);
      }
    });
  };

  Employee.observe('after save', function setRoleMapping(ctx, next) {
    if (ctx.instance) {
      if (ctx.isNewInstance) {
        let Role = Employee.app.models.Role;
        let RoleMapping = Employee.app.models.RoleMapping;

        Role.findOne({
          where: {
            name: 'employee',
          },
        }, function(err, role) {
          if (err) {
            return console.log(err);
          }
          console.log(role);
          RoleMapping.create({
            principalType: 'Employee',
            principalId: ctx.instance.id,
            roleId: role.id,
          }, function(err, principal) {
            if (err)
              throw err;
            console.log('Created Principal: ', principal);
          });
        });
      }
    }
    next();
  });

  Employee.afterRemote('create', function sendEmail(context, employee, next) {
    var options = {
      type: 'email',
      to: employee.email,
      from: 'srinivas@mobcast.in',
      subject: 'Thanks for registering on MarkMe.',
      template: path.resolve(__dirname, '../../server/views/verify.ejs'),
      redirect: '/verified',
      user: employee,
    };

    employee.verify(options, function(err, response) {
      if (err) {
        console.log('Error sending email.');
        Employee.deleteById(employee.id);
        return next(err);
      }
      context.res.render('response', {
        title: 'Signed up successfully',
        content: 'Please check your email and click on the verification link ' +
           'before logging in.',
        redirectTo: '/',
        redirectToLinkText: 'Log in',
      });
      console.log('Email sent with options: ', options);
    });
    next();
  });

  Employee.markIn = function(id, location, cb) {
    let Attendance = Employee.app.models.Attendance;

    let now = new Date();
    let today = new Date(
      Date.UTC(now.getUTCFullYear(), now.getMonth(), now.getDate())
    );

    Employee.findOne({
      where: {
        id: id,
      },
    }, (err, employee) => {
      if (err)
        cb(err);
      console.log(employee);
      // Check to see records in the same day
      Attendance.find({
        where: {
          employeeId: employee.id,
          date: today,
        },
      }, function(err, attendanceRecords) {
        if (err)
          cb(err);

        // Check to see if incomplete attendance cycle
        console.log('records: ', attendanceRecords);
        if (attendanceRecords.length > 0) {
          for (var i = 0; i < attendanceRecords.length; i++) {
            if (attendanceRecords[i].outTime == null) {
              var markInErr = new Error('You have already marked in!');
              markInErr.statusCode = 400;
              cb(markInErr);
            }
          }
          // If control reaches here then there are no incomplete records
          // Create Attendance record
          Attendance.create({
            employeeId: id,
            date: today,
            inTime: now,
            inLocation: location,
          }, (err, attRecord) => {
            if (err)
              return cb(err);
            console.log('Marked IN ', attRecord);
            cb(null, null);
          });
        } else {
          // Create Attendance record
          Attendance.create({
            employeeId: id,
            date: today,
            inTime: now,
            inLocation: location,
          }, (err, attRecord) => {
            if (err)
              cb(err);
            console.log('Marked IN ', attRecord);
            cb(null, null);
          });
        }
      });
    });
  };

  Employee.markOut = function(id, location, cb) {
    let Attendance = Employee.app.models.Attendance;

    let now = new Date();
    let today = new Date(
      Date.UTC(now.getUTCFullYear(), now.getMonth(), now.getDate())
    );

    Employee.findOne({
      where: {
        id: id,
      },
    }, (err, employee) => {
      if (err)
        cb(err);
      console.log(employee);
      // Check to see records in the same day
      Attendance.find({
        where: {
          employeeId: employee.id,
          date: today,
        },
      }, function(err, attendanceRecords) {
        if (err)
          cb(err);

        // Check to see if incomplete attendance cycle
        // TODO: Consider adding a max limit as well.
        console.log('records: ', attendanceRecords);
        if (attendanceRecords.length > 0) {
          for (var i = 0; i < attendanceRecords.length; i++) {
            if (attendanceRecords[i].outTime == null) {
              // Update outTime
              Attendance.update({
                employeeId: employee.id,
                outTime: now,
                outLocation: location,
              }, (err, attRecord) => {
                if (err)
                  cb(err);
                console.log('Marked OUT ', attRecord);
                cb(null, null);
              });
            }
          }
        } else {
          var markInErr = new Error('Please mark in first!');
          markInErr.statusCode = 400;
          cb(markInErr);
        }
      });
    });
  };

  Employee.remoteMethod('mobileLogin', {
    'http': {
      'path': '/mobileLogin',
      'verb': 'post',
    },
    'accepts': [
      {
        'arg': 'credentials',
        'type': 'object',
        'description': 'Login credentials',
        'required': true,
        'http': {
          'source': 'body',
        },
      },
    ],
    'returns': [
      {
        'arg': 'token',
        'type': 'object',
        'root': true,
      },
    ],
  });

  Employee.remoteMethod('markIn', {
    accepts: [
      {
        arg: 'id',
        type: 'String',
        required: true,
      }, {
        arg: 'location',
        type: 'geopoint',
        required: true,
        http: {
          source: 'body',
        },
      },
    ],
    http: {
      path: '/:id/markIn',
      verb: 'post',
    },
    // returns: {arg: '', type: ''},
  });

  Employee.remoteMethod('markOut', {
    accepts: [
      {
        arg: 'id',
        type: 'String',
        required: true,
      }, {
        arg: 'location',
        type: 'geopoint',
        required: true,
        http: {
          source: 'body',
        },
      },
    ],
    http: {
      path: '/:id/markOut',
      verb: 'post',
    },
    // returns: {arg: '', type: ''},
  });
};
