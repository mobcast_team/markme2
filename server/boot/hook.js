'use strict';

module.exports = function(server) {
  var remotes = server.remotes();
  // modify all returned values
  remotes.after('**', function(ctx, next) {
    ctx.result = {
      message: 'Success',
      data: ctx.result,
    };

    next();
  });
};
