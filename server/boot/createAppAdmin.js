'use strict';

module.exports = function(app) {
  var Employee = app.models.Employee;
  var Role = app.models.Role;
  var RoleMapping = app.models.RoleMapping;

  // RoleMapping.defineProperty('principalId', {type: function(id) {
  //   return require('mongodb').ObjectId('' + id);
  // }});

  // Check if admin record already exists
  Employee.findOne({
    where: {
      mobile: '8451820337',
    },
  }, function(err, appAdmin) {
    if (err) {
      throw err;
    }
    if (appAdmin) {
      return; // Do Nothing
    } else {
      // Create App Admin
      Employee.create({
        name: 'Srinivas Vemuri',
        mobile: '8451820337',
        email: 'srinivas@mobcast.in',
        username: '8451820337',
        password: 'abcdef@1234',
        dateOfBirth: Date('1990-09-07T03:24:00'),
      }, function(err, appAdmin) {
        if (err)
          throw err;
        console.log('Created App Admin', appAdmin);

        // Create Role (app admin)
        Role.create({
          name: 'appAdmin',
        }, function(err, role) {
          if (err)
            throw err;
          console.log('Created Role: ', role);
          role.principals.create({
            principalType: 'Employee',
            principalId: appAdmin.id,
            roleId: appAdmin.roleId,
          }, function(err, principal) {
            if (err)
              throw err;
            console.log('Created Principal: ', principal);
          });
        });
      });
    }
  });

  // Create Role (org admin)
  Role.findOne({
    where: {
      name: 'orgAdmin',
    },
  }, (err, role) => {
    if (err)
      throw err;
    if (role) {
      return; // Do Nothing
    } else {
      Role.create({
        name: 'orgAdmin',
      }, function(err, role) {
        if (err)
          throw err;
        console.log('Created Role: ', role);
      });
    }
  });

  // Create Role (group admin)
  Role.findOne({
    where: {
      name: 'groupAdmin',
    },
  }, (err, role) => {
    if (err)
      throw err;
    if (role) {
      return; // Do Nothing
    } else {
      Role.create({
        name: 'groupAdmin',
      }, function(err, role) {
        if (err)
          throw err;
        console.log('Created Role: ', role);
      });
    }
  });

  // Create Role (employee)
  Role.findOne({
    where: {
      name: 'employee',
    },
  }, (err, role) => {
    if (err)
      throw err;
    if (role) {
      return; // Do Nothing
    } else {
      Role.create({
        name: 'employee',
      }, function(err, role) {
        if (err)
          throw err;
        console.log('Created Role: ', role);
      });
    }
  });
};
